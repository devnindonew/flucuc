import 'package:flutter/material.dart';
import 'package:nested_tab/views/home.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Nested Tab',
      home: HomeView(),
    );
  }
}

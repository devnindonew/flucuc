import 'package:flutter/material.dart';
import 'package:nested_tab/components/cards/text_format/title_text.dart';
import 'package:nested_tab/components/style_sheet/color_file.dart';
import 'package:nested_tab/widgets/body_content.dart';

class HomeView extends StatefulWidget {
  @override
  _HomeViewState createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold( 
      appBar: AppBar(
        backgroundColor: ThemeColors.secondaryColor,
        title: TitleText(
          title: "Custom Tab",
          color: ThemeColors.primaryColor,
        ),
      ),
      body: DetailsContent(),
    );
  }
}
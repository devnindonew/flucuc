
import 'package:flutter/material.dart';
import 'package:nested_tab/components/cards/card_design_part/card_icon.dart';
import 'package:nested_tab/components/cards/text_format/button_text.dart';
import 'package:nested_tab/components/style_sheet/color_file.dart';

class PlainButton extends StatelessWidget {
  final String buttonText;
  final icon, color;
  PlainButton({
    this.buttonText,
    this.icon,
    this.color,
  });
  @override
  Widget build(BuildContext context) {
    return FlatButton(
      hoverColor: ThemeColors.grey3,
      onPressed: null,
      child: Center(
        child: (icon == null) && (buttonText != null)
            ? ButtonText(
                buttonText: buttonText,
                textColor: color,
              )
            : (icon != null) && (buttonText == null)
                ? CardIcon(
                    iconName: icon,
                    color: color,
                  )
                : Row(
                    children: <Widget>[
                      CardIcon(
                        iconName: icon,
                        color: color,
                      ),
                      ButtonText(
                        buttonText: buttonText,
                        textColor: color,
                      ),
                    ],
                  ),
      ),
    );
  }
}

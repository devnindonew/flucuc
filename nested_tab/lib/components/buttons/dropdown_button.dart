
import 'package:flutter/material.dart';
import 'package:nested_tab/components/cards/text_format/button_text.dart';
import 'package:nested_tab/components/style_sheet/color_file.dart';

class BasicDropdown extends StatefulWidget {
  @override
  _BasicDropdownState createState() => _BasicDropdownState();
}

class _BasicDropdownState extends State<BasicDropdown> {
  List _options = ["A", "B", "C", "D"];
  var _selectedOption;
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(5),
      decoration: ShapeDecoration(
        shape: RoundedRectangleBorder(
          side: BorderSide(width: 1.0, style: BorderStyle.solid),
          borderRadius: BorderRadius.all(
            Radius.circular(5.0),
          ),
        ),
      ),
      child: Center(
        child: DropdownButton(
          isExpanded: true,
          hint: ButtonText(
            buttonText: "Select a option",
            textColor: ThemeColors.grey8,
            buttonFontSize: 20.0,
          ),
          value: _selectedOption,
          onChanged: (value) {
            setState(() {
              _selectedOption = value;
            });
          },
          items: _options.map((option) {
            return DropdownMenuItem(
              child: ButtonText(
                buttonText: option,
                buttonFontSize: 16.0,
                textColor: ThemeColors.grey8,
              ),
              value: option,
            );
          }).toList(),
        ),
      ),
    );
  }
}

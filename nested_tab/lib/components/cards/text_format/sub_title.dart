import 'package:flutter/material.dart';

class SubTitle extends StatelessWidget {
  final String subtitle;
  SubTitle({
    this.subtitle
  });
  @override
  Widget build(BuildContext context) {
    return Text(
      subtitle != null ? subtitle : "",
      style: TextStyle(fontSize: 20, fontWeight: FontWeight.w400),
    );
  }
}

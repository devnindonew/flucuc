import 'package:flutter/material.dart';
import 'package:nested_tab/components/style_sheet/color_file.dart';

class ParagraphText extends StatelessWidget {
  final String body;
  ParagraphText({
    this.body,
  });
  @override
  Widget build(BuildContext context) {
    return Text(
      (body != null ) ? body : "",
      textAlign: TextAlign.justify,
      style: TextStyle(fontSize: 20, fontWeight: FontWeight.w300, color: ThemeColors.secondaryColor),
    );
  }
}

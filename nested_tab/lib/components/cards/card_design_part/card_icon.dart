
import 'package:flutter/material.dart';
import 'package:nested_tab/components/style_sheet/color_file.dart';
import 'package:nested_tab/components/style_sheet/size_file.dart';

class CardIcon extends StatelessWidget {
  final iconName, iconSize;
  final color;
  CardIcon({
    this.iconName,
    this.color,
    this.iconSize,
  });
  @override
  Widget build(BuildContext context) {
    return Icon(
      iconName,
      color: color != null ? color : ThemeColors.grey1,
      size: iconSize != null ? iconSize : FontSize.medium,
    );
  }
}

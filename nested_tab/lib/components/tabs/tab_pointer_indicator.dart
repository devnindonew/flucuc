
import 'package:flutter/material.dart';
import 'package:nested_tab/components/cards/text_format/title_text.dart';

import 'tab_indicator/custom_tab_indicator.dart';
import 'tab_indicator/dot_tab_indicator.dart';

class TabPointer extends StatefulWidget {
  @override
  _TabPointerState createState() => _TabPointerState();
}

class _TabPointerState extends State<TabPointer> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          title: TitleText(
            title: "Tab Pointer indicator",
          ),
          backgroundColor: Colors.black,
          bottom: TabBar(
            indicator: CustomTabIndicator(),
            //indicator: DotTabIndicator(color: ThemeColors.grey1, radius: 3),
            tabs: <Widget>[
              Tab(
                child: Text("One"),
              ),
              Tab(
                child: Text("Two"),
              ),
              Tab(
                child: Text("Three"),
              ),
            ],
          ),
        ),
        body: TabBarView(
          children: <Widget>[
            Center(
              child: Text("Tab 1"),
            ),
            Center(
              child: Text("Tab 2"),
            ),
            Center(
              child: Text("Tab 3"),
            ),
          ],
        ),
      ),
    );
  }
}


import 'package:flutter/material.dart';
import 'package:nested_tab/components/style_sheet/color_file.dart';

class CardDesign extends StatelessWidget {
  final customWidget;
  CardDesign({
    this.customWidget,
  });
  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
      margin: EdgeInsets.fromLTRB(15, 5, 15, 5),
      decoration: BoxDecoration(
        color: ThemeColors.primaryColor,
        borderRadius: BorderRadius.circular(5),
        boxShadow: [
          BoxShadow(
            color: ThemeColors.grey5,
            blurRadius: 10,
            spreadRadius: 5,
            offset: Offset(3, 3),
          ),
        ],
      ),
      child: customWidget,
    );
  }
}

import 'package:flutter/material.dart';
import 'package:sidebar_menu/sidebar/sidebar_menu.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: MenuDashboardPage(),
    );
  }
}

import '../navigation/navigation_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'menu_item.dart';

class SidebarMenu extends StatelessWidget {
  final Function onPresse;

  const SidebarMenu({Key key, this.onPresse}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 20),
        color: Colors.black,
        child: Column(
          children: <Widget>[
            SizedBox(
              height: 80,
            ),
            ListTile(
              title: Text(
                "Name",
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 30,
                  fontWeight: FontWeight.w800,
                ),
              ),
              subtitle: Text(
                "Email",
                style: TextStyle(
                  color: Colors.grey[500],
                  fontSize: 24,
                  fontWeight: FontWeight.w300,
                ),
              ),
              leading: CircleAvatar(
                backgroundColor: Colors.white,
                child: Icon(
                  Icons.perm_identity,
                  color: Colors.grey[800],
                  size: 38,
                ),
                radius: 30,
              ),
            ),
            Divider(
              height: 60,
              thickness: 1.5,
              color: Colors.grey[500],
              indent: 20,
              endIndent: 20,
            ),
            MenuItem(
              icon: Icons.home,
              title: "Home",
              onTap: () {
                onPresse();
                BlocProvider.of<NavigationBloc>(context)
                    .add(NavigationEvents.HomePageClickEvent);
              },
            ),
            MenuItem(
              icon: Icons.account_box,
              title: "Account",
              onTap: () {
                onPresse();
                BlocProvider.of<NavigationBloc>(context)
                    .add(NavigationEvents.AccountPageClickEvent);
              },
            ),
            MenuItem(
              icon: Icons.shopping_cart,
              title: "Order",
              onTap: () {
                onPresse();
                BlocProvider.of<NavigationBloc>(context)
                    .add(NavigationEvents.OrderPageClickEvent);
              },
            ),
            Divider(
              height: 60,
              thickness: 1.5,
              color: Colors.grey[500],
              indent: 20,
              endIndent: 20,
            ),
            MenuItem(
              icon: Icons.settings,
              title: "Settings",
            ),
            MenuItem(
              icon: Icons.exit_to_app,
              title: "Logout",
            ),
          ],
        ),
      ),
    );
  }
}

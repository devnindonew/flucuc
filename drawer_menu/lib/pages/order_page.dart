import '../navigation/navigation_bloc.dart';
import 'package:flutter/material.dart';

class OrderPage extends StatelessWidget with NavigationState{
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text(
        "Order",
        style: TextStyle(fontWeight: FontWeight.w900, fontSize: 32),
      ),
    );
  }
}
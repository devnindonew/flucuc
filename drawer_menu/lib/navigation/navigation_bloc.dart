import 'package:bloc/bloc.dart';
import '../pages/account_page.dart';
import '../pages/home_page.dart';
import '../pages/order_page.dart';

enum NavigationEvents {
  HomePageClickEvent,
  AccountPageClickEvent,
  OrderPageClickEvent,
}

abstract class NavigationState {}

class NavigationBloc extends Bloc<NavigationEvents, NavigationState> {
  @override
  NavigationState get initialState => HomePage();

  @override
  Stream<NavigationState> mapEventToState(NavigationEvents event) async* {
    switch (event) {
      case NavigationEvents.HomePageClickEvent:
        yield HomePage();
        break;
      case NavigationEvents.AccountPageClickEvent:
        yield AccountPage();
        break;
      case NavigationEvents.OrderPageClickEvent:
        yield OrderPage();
        break;
    }
  }
}

import 'package:custom_components/components/cards/basic_plain_card.dart';
import 'package:custom_components/components/chips/choice_chip.dart';
import 'package:custom_components/components/chips/input_chip.dart';
import 'package:custom_components/components/chips/multi_select_chip.dart';
import 'package:custom_components/components/style_sheet/color_file.dart';
import 'package:flutter/material.dart';

class ChipsView extends StatefulWidget {
  @override
  _ChipsViewState createState() => _ChipsViewState();
}

class _ChipsViewState extends State<ChipsView> {
  List choiceList = ["Flutter", "Dart", "CSS", "JAVA", "JavaScript"];
  List selectedChoiceList;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Flutter chip",
          style: TextStyle(
            color: ThemeColors.grey3,
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          child: Column(
            children: <Widget>[
              BasicPlainCard(
                title: "Input Chip",
                subtitle: "Represent a piece of information",
                body:
                    "can be made selectable by setting onSelected, deletable by setting onDeleted, and pressable like a button with onPressed.",
                customWidget: Center(
                  child: InputChipsDemo(
                    chipText: "Flutter",
                  ),
                ),
              ),
              BasicPlainCard(
                title: "Choice Chip",
                subtitle: "Option selectable chip",
                body:
                    "represent a single choice from a set. Choice chips contain related descriptive text or categories.",
                customWidget: Container(
                  child: ChoiceChipDemo(
                    choiceList: choiceList,
                  ),
                ),
              ),
              BasicPlainCard(
                title: "Select Chip",
                subtitle: "Multi select chip",
                body: "can select multiple choice from options",
                customWidget: Container(
                  child: MultiselectChip(
                    choiceList: choiceList,
                    onSelectedChanged: (selectedList) {
                      setState(() {
                        selectedChoiceList = selectedList;
                      });
                    },
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

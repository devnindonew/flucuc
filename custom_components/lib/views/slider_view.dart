import 'package:custom_components/components/sliders/basic_slider.dart';
import 'package:custom_components/components/sliders/feedback_slider.dart';
import 'package:custom_components/components/sliders/range_slider.dart';
import 'package:custom_components/components/sliders/rating_slider.dart';
import 'package:custom_components/components/style_sheet/color_file.dart';
import 'package:flutter/material.dart';

class SliderView extends StatefulWidget {
  @override
  _SliderViewState createState() => _SliderViewState();
}

class _SliderViewState extends State<SliderView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Slider",
          style: TextStyle(color: ThemeColors.grey2),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            FeedbackSlider(),
            RatingSlider(),
            SliderRange(),
            BasicSlider(),
          ],
        ),
      ),
    );
  }
}

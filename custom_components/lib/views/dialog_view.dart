import 'package:custom_components/components/cards/card_with_button.dart';
import 'package:custom_components/components/dialog/alert_dialog.dart';
import 'package:custom_components/components/dialog/image_dialog.dart';
import 'package:custom_components/components/dialog/input_dialog.dart';
import 'package:custom_components/components/dialog/selection_dialog.dart';
import 'package:custom_components/components/dialog/slider_dialog.dart';
import 'package:custom_components/components/style_sheet/color_file.dart';
import 'package:flutter/material.dart';

class DialogView extends StatefulWidget {
  @override
  _DialogViewState createState() => _DialogViewState();
}

class _DialogViewState extends State<DialogView> {
  TextEditingController customController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Dialog",
          style: TextStyle(color: ThemeColors.grey2),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            CardWithButton(
              title: "Alert Dialog",
              subTitle: "Pop-up alert message",
              body: "Alert message in a pop-up dialog box with a button",
              button1text: "Alert",
              onpressedButton1: () {
                showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return DialogAlert();
                  },
                );
              },
            ),
            SizedBox(
              height: 5,
            ),
            CardWithButton(
              title: "Image Alert",
              subTitle: "Pop-up Image dialog box",
              body: "Image alert pop-up dialog box with a button",
              button1text: "Dialog",
              onpressedButton1: () {
                showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return DialogImage();
                  },
                );
              },
            ),
            SizedBox(
              height: 5,
            ),
            CardWithButton(
              title: "Input Dialog",
              subTitle: "Input pop-up dialog box",
              body: "Pop-up dialog box, with user input field",
              button1text: "Input",
              onpressedButton1: () {
                showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return DialogInput(
                      customController: customController,
                    );
                  },
                );
              },
            ),
            SizedBox(
              height: 5,
            ),
            CardWithButton(
              title: "Selection Dialog",
              subTitle: "Dialog with option",
              body: "Pop-up dialog box with option selection",
              button1text: "Select",
              onpressedButton1: () {
                showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return SelectionDialog();
                  },
                );
              },
            ),
            SizedBox(
              height: 5,
            ),
            CardWithButton(
              title: "Slider Dialog",
              subTitle: "Pop-up dialog with slider",
              body: "Dialog box with a slider option to set value",
              button1text: "Slider",
              onpressedButton1: (){
                showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return SliderDialog();
                  },
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}

import 'package:custom_components/components/cards/basic_plain_card.dart';
import 'package:custom_components/components/cards/card_with_button.dart';
import 'package:custom_components/components/cards/text_format/title_text.dart';
import 'package:custom_components/components/progress_bar/circle_progress_bar.dart';
import 'package:custom_components/components/progress_bar/circle_progress_card.dart';
import 'package:custom_components/components/style_sheet/color_file.dart';
import 'package:flutter/material.dart';

class ProgressBarView extends StatefulWidget {
  @override
  _ProgressBarViewState createState() => _ProgressBarViewState();
}

class _ProgressBarViewState extends State<ProgressBarView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: TitleText(
          title: "Progress Bar",
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            BasicPlainCard(
              title: "Circular Progress Bar",
              subtitle: "Flutter progress bar",
              body: "Flutter Circular Progress Indicator Example",
              customWidget: Container(
                margin: EdgeInsets.all(25),
                child: CircleProgressCard(),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

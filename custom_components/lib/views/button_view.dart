import 'package:custom_components/components/buttons/animated_button.dart';
import 'package:custom_components/components/buttons/custom_icon_button.dart';
import 'package:custom_components/components/buttons/dropdown_button.dart';
import 'package:custom_components/components/buttons/primary_button.dart';
import 'package:custom_components/components/buttons/progress_button.dart';
import 'package:custom_components/components/buttons/secondary_button.dart';
import 'package:custom_components/components/cards/basic_plain_card.dart';
import 'package:custom_components/components/style_sheet/color_file.dart';
import 'package:custom_components/components/style_sheet/shape_file.dart';
import 'package:custom_components/components/style_sheet/size_file.dart';
import 'package:flutter/material.dart';

class ButtonView extends StatefulWidget {
  @override
  _ButtonViewState createState() => _ButtonViewState();
}

class _ButtonViewState extends State<ButtonView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Button",
          style: TextStyle(
            color: ThemeColors.grey3,
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          child: Column(
            children: <Widget>[
              BasicPlainCard(
                title: "Primary Button",
                customWidget: Column(
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        PrimaryButton(
                          buttonText: "Square",
                          color: ThemeColors.teal7,
                          buttonShape: ShapeStyle.square,
                          shadowColor: ThemeColors.grey9,
                        ),
                        PrimaryButton(
                          buttonText: "Circuler",
                          color: ThemeColors.green7,
                          buttonShape: ShapeStyle.circuler,
                          shadowColor: ThemeColors.grey9,
                        )
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        PrimaryButton(
                          buttonText: "Rectangal",
                          color: ThemeColors.grey7,
                          buttonShape: ShapeStyle.rectanguler,
                          shadowColor: ThemeColors.grey9,
                        ),
                        PrimaryButton(
                          buttonText: "Rounded",
                          color: ThemeColors.purple6,
                          buttonShape: ShapeStyle.roundedCircle,
                          shadowColor: ThemeColors.grey9,
                        )
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        PrimaryButton(
                          icon: Icons.shopping_basket,
                          buttonText: "Oval",
                          color: ThemeColors.red9,
                          buttonShape: ShapeStyle.oval,
                          shadowColor: ThemeColors.grey9,
                        ),
                        PrimaryButton(
                          icon: Icons.shutter_speed,
                          color: ThemeColors.purple6,
                          buttonShape: ShapeStyle.circuler,
                          shadowColor: ThemeColors.grey9,
                        ),
                      ],
                    )
                  ],
                ),
              ),
              SizedBox(
                height: 10,
              ),
              BasicPlainCard(
                title: "Secondary Button",
                customWidget: Column(
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        SecondaryButton(
                          buttonText: "Square",
                          color: ThemeColors.red9,
                          buttonShape: ShapeStyle.square,
                        ),
                        SecondaryButton(
                          buttonText: "Rectangal",
                          color: ThemeColors.green9,
                          buttonShape: ShapeStyle.rectanguler,
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        SecondaryButton(
                          buttonText: "Circuler",
                          icon: Icons.copyright,
                          buttonShape: ShapeStyle.circuler,
                          color: ThemeColors.teal9,
                        ),
                        SecondaryButton(
                          buttonText: "Rounded",
                          buttonShape: ShapeStyle.roundedCircle,
                          color: ThemeColors.purple9,
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        SecondaryButton(
                          buttonText: "Oval",
                          buttonShape: ShapeStyle.oval,
                          color: ThemeColors.purple5,
                        ),
                        SecondaryButton(
                          icon: Icons.credit_card,
                          buttonShape: ShapeStyle.oval,
                          color: ThemeColors.grey9,
                        )
                      ],
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 10,
              ),
              BasicPlainCard(
                title: "Button Size",
                customWidget: Column(
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        PrimaryButton(
                          buttonText: "Small",
                          buttonShape: ShapeStyle.roundedCircle,
                          buttonFontSize: FontSize.small,
                          buttonWidth: ButtonCustomSize.widthSmall,
                          buttonHeight: ButtonCustomSize.heightSmall,
                          color: ThemeColors.green7,
                        ),
                        PrimaryButton(
                          buttonText: "Ultra",
                          buttonShape: ShapeStyle.rectanguler,
                          buttonFontSize: FontSize.ultraLarge,
                          buttonWidth: ButtonCustomSize.widthUltraLarge,
                          buttonHeight: ButtonCustomSize.heightUltraLarge,
                          color: ThemeColors.teal4,
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        PrimaryButton(
                          buttonText: "Medium",
                          buttonShape: ShapeStyle.square,
                          buttonFontSize: FontSize.medium,
                          buttonWidth: ButtonCustomSize.widthMedium,
                          buttonHeight: ButtonCustomSize.heightMedium,
                          color: ThemeColors.teal7,
                        ),
                        PrimaryButton(
                          buttonText: "Large",
                          buttonShape: ShapeStyle.circuler,
                          buttonFontSize: FontSize.xlarge,
                          buttonWidth: ButtonCustomSize.widthLarge,
                          buttonHeight: ButtonCustomSize.heightLarge,
                          color: ThemeColors.purple7,
                        )
                      ],
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        PrimaryButton(
                          buttonText: "Regular",
                          buttonShape: ShapeStyle.oval,
                          buttonFontSize: FontSize.regular,
                          buttonWidth: ButtonCustomSize.widthRegular,
                          buttonHeight: ButtonCustomSize.heightRegular,
                          color: ThemeColors.purple4,
                        ),
                        PrimaryButton(
                          buttonText: "XLarge",
                          buttonShape: ShapeStyle.roundedCircle,
                          buttonFontSize: FontSize.xxlarge,
                          buttonWidth: ButtonCustomSize.widthXLarge,
                          buttonHeight: ButtonCustomSize.heightXLarge,
                          color: ThemeColors.red8,
                        )
                      ],
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 10,
              ),
              BasicPlainCard(
                title: "Icon Button",
                customWidget: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    CustomIconButton(
                      buttonSize: 40.0,
                      iconSize: 18.0,
                      iconName: Icons.contact_phone,
                    ),
                    CustomIconButton(
                      buttonSize: 50.0,
                      iconSize: 26.0,
                      iconName: Icons.credit_card,
                    ),
                    CustomIconButton(
                      buttonSize: 55.0,
                      iconSize: 30.0,
                      iconName: Icons.crop_original,
                    ),
                    CustomIconButton(
                      buttonSize: 60.0,
                      iconSize: 34.0,
                      iconName: Icons.dashboard,
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 10,
              ),
              BasicPlainCard(
                title: "Progress Button",
                customWidget: Column(
                  children: <Widget>[
                    ProgressButton(
                      buttonText: "Click Here",
                    ),
                    SizedBox(height: 5),
                    AnimatedButton(),
                  ],
                ),
              ),
              SizedBox(
                height: 10,
              ),
              BasicPlainCard(
                title: "Dropdwon Button",
                customWidget: Column(
                  children: <Widget>[
                    BasicDropdown(),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

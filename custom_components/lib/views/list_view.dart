
import 'package:custom_components/components/style_sheet/color_file.dart';
import 'package:flutter/material.dart';

class ListsView extends StatefulWidget {
  @override
  _ListsViewState createState() => _ListsViewState();
}

class _ListsViewState extends State<ListsView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "List",
          style: TextStyle(
            color: ThemeColors.grey3,
          ),
        ),
      ),
      body: Container(),
    );
  }
}

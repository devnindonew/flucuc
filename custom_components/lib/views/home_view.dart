import 'package:custom_components/components/cards/basic_plain_card.dart';
import 'package:custom_components/components/list_view/basic_list.dart';
import 'package:custom_components/components/style_sheet/color_file.dart';
import 'package:flutter/material.dart';

class HomeView extends StatefulWidget {
  @override
  _HomeViewState createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> {
  List<String> listItem = [
    "Card",
    "List",
    "Button",
    "Tab",
    "Chip",
    "Dialog",
    "Slider",
    "Progress",
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Flutter custom components",
          style: TextStyle(
            color: ThemeColors.grey3,
          ),
        ),
      ),
      body: Container(
        child: Column(
          children: <Widget>[
            BasicPlainCard(
              title: "Custom Components",
              subtitle: "Flutter",
              body:
                  "Flutter custom created components. This helps to do less code by reuse same component.",
            ),
            Expanded(
              child: BasicList(
                listTitle: "Component List",
                items: listItem,
              ),
            ),
          ],
        ),
      ),
    );
  }
}

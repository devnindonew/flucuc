import 'package:custom_components/components/cards/basic_plain_card.dart';
import 'package:custom_components/components/cards/card_image_icon.dart';
import 'package:custom_components/components/cards/card_with_button.dart';
import 'package:custom_components/components/cards/card_with_image.dart';
import 'package:custom_components/components/cards/content_card.dart';
import 'package:custom_components/components/cards/expandable_card/expandable_card.dart';
import 'package:custom_components/components/cards/image_title_card.dart';
import 'package:custom_components/components/cards/media_card.dart';
import 'package:custom_components/components/cards/vertical_image_icon_card.dart';
import 'package:custom_components/components/style_sheet/color_file.dart';
import 'package:flutter/material.dart';

class CardView extends StatefulWidget {
  @override
  _CardViewState createState() => _CardViewState();
}

class _CardViewState extends State<CardView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Card",
          style: TextStyle(
            color: ThemeColors.grey3,
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            BasicPlainCard(
              title: "Basic Card",
              subtitle: "Card without button",
              body: "Basic plain card design without any button",
            ),
            SizedBox(
              height: 10,
            ),
            CardWithButton(
              title: "Action Card",
              subTitle: "Card with buttons",
              body: "Basic flutter card design with action buttons",
              button1text: "Xplore",
              button2text: "Details",
            ),
            SizedBox(
              height: 10,
            ),
            CardWithImage(
              title: "Image Card",
              subTitle: "Card with Image",
              body: "Basic flutter card with image and action button",
              asset: "assets/pc.png",
              buttonText: "Details",
            ),
            SizedBox(
              height: 10,
            ),
            ImageIconCard(
              asset: "assets/photo.jpg",
              title: "Image Icon Card",
              icon1: Icons.favorite,
              color1: Colors.redAccent,
              icon2: Icons.motorcycle,
              color2: Colors.teal,
              icon3: Icons.message,
              color3: Colors.purpleAccent,
            ),
            SizedBox(
              height: 10,
            ),
            VerticalImageIconCard(
              asset: "assets/photo.jpg",
              icon1: Icons.favorite,
              color1: Colors.redAccent,
              icon2: Icons.motorcycle,
              color2: Colors.teal,
              icon3: Icons.message,
              color3: Colors.purpleAccent,
            ),
            SizedBox(
              height: 10,
            ),
            ContentCard(
              asset: "assets/dhaka.jpg",
              iconName1: Icons.restaurant,
              color1: Colors.green[900],
              iconName2: Icons.home,
              color2: Colors.red[900],
              title1: "Restaurant",
              subtitle1: "50 near by restaurant",
              title2: "Hotel",
              subtitle2: "28 near by hotel",
            ),
            SizedBox(
              height: 10,
            ),
            ImageTitleCard(
              asset: "assets/bangladesh.PNG",
              title: "Media Card",
              body:
                  "Bangladesh cricket team jersey, details and achievement in ICC world cup event",
            ),
            SizedBox(
              height: 10,
            ),
            MediaCard(
              asset: "assets/dhaka.jpg",
              buttonText: "Explore",
              body:
                  "Dhaka is the capital city of Bangladesh, in southern Asia. Set beside the Buriganga River. ",
              icon1: Icons.location_city,
              color1: Colors.green[900],
              subtitle1: "Dhaka",
              subtitle2: "Bangladesh",
            ),
            SizedBox(
              height: 10,
            ),
            ExpandedCard(),
          ],
        ),
      ),
    );
  }
}

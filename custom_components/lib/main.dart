import 'package:custom_components/components/progress_bar/circle_progress_bar.dart';
import 'package:custom_components/views/button_view.dart';
import 'package:custom_components/views/card_view.dart';
import 'package:custom_components/views/chips_view.dart';
import 'package:custom_components/views/dialog_view.dart';
import 'package:custom_components/views/home_view.dart';
import 'package:custom_components/views/list_view.dart';
import 'package:custom_components/views/progress_bar_view.dart';
import 'package:custom_components/views/slider_view.dart';
import 'package:custom_components/views/tab_view.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      routes: <String, WidgetBuilder>{
        "/Card": (BuildContext context) => CardView(),
        "/List": (BuildContext context) => ListsView(),
        "/Button": (BuildContext context) => ButtonView(),
        "/Tab": (BuildContext context) => TabView(),
        "/Chip": (BuildContext context) => ChipsView(),
        "/Dialog": (BuildContext context) => DialogView(),
        "/Slider": (BuildContext context) => SliderView(),
        "/Progress": (BuildContext context) => ProgressBarView(),
        "/circularBar": (BuildContext context) => CircleProgressBar(),
      },
      title: 'Flutter Demo',
      theme: ThemeData(primaryColor: Colors.black),
      home: HomeView(),
    );
  }
}

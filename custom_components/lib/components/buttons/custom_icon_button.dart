
import 'package:custom_components/components/cards/card_design_part/card_icon.dart';
import 'package:custom_components/components/style_sheet/color_file.dart';
import 'package:flutter/material.dart';

class CustomIconButton extends StatelessWidget {
  final iconName, buttonSize, iconSize;
  CustomIconButton({
    this.iconName,
    this.buttonSize,
    this.iconSize,
  });
  @override
  Widget build(BuildContext context) {
    return Container(
      child: RawMaterialButton(
        fillColor: ThemeColors.purple9,
        shape: CircleBorder(),
        onPressed: null,
        constraints: BoxConstraints.tight(
          Size(buttonSize != null ? buttonSize : 50, 60),
        ),
        child: CardIcon(
          iconName: iconName,
          iconSize: iconSize,
          
        ),
      ),
    );
  }
}
